#ifndef __LEDC_APP_H
#define __LEDC_APP_H

void Ledc_Init(void);
void Ledc_App_Pin(int pin, int channel);
void Ledc_Set_Duty(int channel, int duty);

#endif
